
export default class Match {

	constructor(matchId, league, teams, matchType, matchDate, isSquadSelected, tossWinner, winnerTeam, matchStarted, image) {

		this.matchId = matchId;
		this.league = "";
		this.teams = teams;
		this.matchType = matchType;
		this.team1 = teams[0];
		this.team2 = teams[1];
		this.matchDate = matchDate;
		this.isSquadSelected = isSquadSelected;
		this.tossWinner = tossWinner;
		this.winnerTeam = winnerTeam;
		this.matchStarted = matchStarted;
		this.image = "";

	}

	getMatchId() {
		return this.matchId;
	}

	getLeague () {
		return this.league;
	}

	getTeams() {
		return this.teams;
	}

	getMatchType() {
		return this.matchType;
	}

	getTeam1() {
		return this.team1;
	}

	getTeam2() {
		return this.team2;
	}

	getMatchDate() {
		return this.matchDate;
	}

	getIsSquadSelected() {
		return this.isSquadSelected;
	}

	getTossWinner() {
		return this.tossWinner;
	}

	getWinnerTeam() {
		return this.winnerTeam;
	}

	getMatchStarted() {
		return this.matchStarted;
	}
};