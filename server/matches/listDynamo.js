import uuid from "uuid";
import AWS from "aws-sdk";

import * as dynamoDbLib from "./../libs/dynamodb-lib.js";
import {success, failure} from "./../libs/response-lib.js";


AWS.config.update({ region : process.env.AWS_REGION });

const dynamoDb = new AWS.DynamoDB.DocumentClient();


export async function listMatches(event, context, callback) {

	const params = {
		TableName : "user_profile",
		KeyConditionExpression: "user_id = :userId",
		ExpressionAttributeValues: {
 			":userId": event.requestContext.identity.cognitoIdentityId
 		}
	};

	try {
		const result = await dynamoDbLib.call("query", params);
		if(result.item) {
		 	var obj = { status : true, "name":"John", "age":30, "city":"New York"};
			var body = JSON.stringify(obj);
			callback(null, success(body));

		} else {
			callback(null, failure({status : false, message : "User not found!" }));
		}

	} catch(e) {
		console.log(e);
		callback(null, failure({status : false, message : "Internal Error"}));
	}

	// var obj = { status : true, "name":"John", "age":30, "city":"New York"};
	// var body = JSON.stringify(obj);
	// callback(null, success(body));

}