import uuid from "uuid";
import AWS from "aws-sdk";
import request from "request";
import _ from "lodash";
import moment from "moment";
import Match from "../classes/Match.js";
import {success, failure} from "./../libs/response-lib.js";

export async function listMatches(event, context, callback) {

	try {

		await request({
			
			headers : {
				"apiKey" : "bQvMKzwOyZfvDHxf8JHmpV4Jq2o2", //process.ENV.CRIC_API_KEY
			},
			uri : "http://cricapi.com/api/matches/", //process.ENV.CRIC_API_URI,

		}, function(err, response, body) {
			
			if(null !== err) {
				return callback(null, failure({ status : false, message : "Error fetching Match List!" }));
			}

			var data = JSON.parse(body).matches;
			var matches = [];
			var length = data.length;

			var matchTypeMap = { };

			for(var i = 0; i < length; i++) {

				var matchRes = data[i];

				var tossWinner = "";
				var winnerTeam = "";
				var matchImage = "";

				//Exclude matches that are started already!
				if(matchRes["matchStarted"] == true) {
			 			continue;
				}

				//Exlude where match teams are not fixed.
				if("tba" == matchRes["team-1"].toLowerCase() || "tba" == matchRes["team-2"]) {
					continue;
				}

				//Skip if cannot find type. IPL will always have type
				if(null == matchRes["type"] || 0 == matchRes["type"].length) {
					continue;
				}

				matchTypeMap[matchRes["type"]] = true;
				//matchTypeMap.set(matchRes["type"], true);

				var matchArr = [
					matchRes["unique_id"],
					"",
					[ matchRes["team-2"], matchRes["team-1"]],
					matchRes["type"],
					matchRes["date"],
					matchRes["squad"],
					tossWinner,
					winnerTeam,
					matchRes["matchStarted"],
					matchImage
				];

				var match = new Match(...matchArr);
				matches.push(match);
			};

			//Sort by earliest first and get first 10 matches
			matches = _.sortBy(matches, function(o) { return new moment(o.matchDate).format('YYYYMMDD'); });
			matches = _.slice(matches, 0, 10);
			
			//Just send the Match Type also, so that a filter can be added if required!
			var matchTypeList = []; 
			_.forEach(matchTypeMap, function(value, key) {
				//console.log("Key : " + key + "::: Value " + value);
				matchTypeList.push(key);
			});

			return callback(null, success({ status : true, data : { matches : matches, matchTypeList : matchTypeList }}));
			

		});

	} catch(e) {
		console.log(e);
		return callback(null, failure({status : false, message : "Internal Error"}));
	}
}