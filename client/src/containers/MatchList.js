import React, { Component } from "react";
import { Link } from "react-router-dom";
import { PageHeader, ListGroup, ListGroupItem, Grid, Row, Col } from "react-bootstrap";
import { invokeApig } from '../libs/awsLib';
import "./MatchList.css";

export default class MatchList extends Component {
	
	constructor(props) {
    	super(props);

    	this.state = {
      		isLoading: true,
      		matches: [],
      		matchTypes : []
    	};
  	}

  	async componentDidMount() {
	    
	    try {
	      const results = await this.upcomingMatches();
	      this.setState({ matches: results.data.matches, matchTypes : results.data.matchTypes });
	    } catch (e) {
	      alert(e);
	    }

	    this.setState({ isLoading: false });
	}


  upcomingMatches() {
    	var result =  invokeApig({ path: "/matches", allowGuest : true });
      return result;
  }

  renderMatchList(matches) {
    // var r =  [{}].concat(matches).map(
    //   (match, i) =>
    //     { match.team1 && match.team2} ?
    //     <ListGroupItem
    //         key={match.matchId}
    //         href={`/match/${match.matchId}`}
    //         onClick={this.handleMatchClick}
    //     >
    //       <h6> {match.team1 + " vs. " + match.team2} </h6>
    //     </ListGroupItem>  
    //     : ""
    // );
    // return r;
  }

  renderNoMatches() {
    return (<b>Loading Upcoming Matches...</b>);
  }

  handleMatchClick = event => {
    event.preventDefault();
    this.props.history.push(event.currentTarget.getAttribute("href"));
  }

  

  render() {
    return (
      <div className="">
        {this.state.matches.length > 0 
          ? <ListGroup>  {this.state.matches.map(match => (
                            <ListGroupItem
                              key={match.matchId}><h6> {match.team1 + " vs. " + match.team2} </h6>
                            </ListGroupItem>  
            ))} </ListGroup> 
          : this.renderNoMatches()
        }
      </div>
    );
  }

}