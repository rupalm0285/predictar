import React, { Component } from "react";
import { Link } from "react-router-dom";
import { PageHeader, ListGroup, ListGroupItem, Grid, Row, Col } from "react-bootstrap";
import { Tabs, Tab} from "react-bootstrap";
import { invokeApig } from '../libs/awsLib';
import "./Home.css";
import MatchList from "../containers/MatchList";

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
    };
  }


  renderLanderNew() {
    return (
      <div className="lander">
        <h1>Welcome to your Dashboard!</h1>
        <Grid fluid className="main-wrapper">
          <Row>
            <Col md={12} sm={12}>
              <div className="card card-nav-tabs">
                <div className="card-header card-header-match-list">
                  <div className="nav-tabs-navigation">
                    <div className="nav-tabs-wrapper">
                      <ul className="nav nav-tabs upcoming-matches-wrapper" data-tabs="tabs">
                        <li className="nav-item" data-tab="signup">
                          <a className="nav-link active" href="#upcoming-matches" data-toggle="tab">
                            Upcoming Matches
                          </a>
                        </li>
                      </ul>
                    </div>                                                                                                                                               
                  </div>                  
                </div>
                <div className="card-body">
                  <div className="tab-content text-center">
                    <div className="tab-pane active" id="upcoming-matches">
                      <MatchList />
                    </div>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }

  render() {
    return (
      <div className="Home">
        {this.renderLanderNew()}
      </div>
    );
  }
}
