import React, { Component } from "react";
import {
  CognitoUserPool,
  AuthenticationDetails,
  CognitoUser
} from "amazon-cognito-identity-js";
import { FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import LoaderButton from "../components/LoaderButton";
import config from "../config";
import "./Login.css";

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      emailLogin: "",
      passwordLogin: ""
    };
  }

  login(emailLogin, passwordLogin) {
    const userPool = new CognitoUserPool({
      UserPoolId: config.cognito.USER_POOL_ID,
      ClientId: config.cognito.APP_CLIENT_ID
    });
    const user = new CognitoUser({ Username: emailLogin, Pool: userPool });
    const authenticationData = { Username: emailLogin, Password: passwordLogin };
    const authenticationDetails = new AuthenticationDetails(authenticationData);

    return new Promise((resolve, reject) =>
      user.authenticateUser(authenticationDetails, {
        onSuccess: result => resolve(),
        onFailure: err => reject(err)
      })
    );
  }

  validateForm() {
    return this.state.emailLogin.length > 0 && this.state.passwordLogin.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit = async event => {
    event.preventDefault();

    this.setState({ isLoading: true });

    try {
      await this.login(this.state.emailLogin, this.state.passwordLogin);
      this.props.userHasAuthenticated(true);
      this.props.history.push("/dashboard");
    } catch (e) {
      alert(e);
      console.log(e);
      this.setState({ isLoading: false });
    }
  }

  render() {
    return (
      <div className="Login">
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="emailLogin" bsSize="large" className="text-left">
            <ControlLabel className="form-label">Email</ControlLabel>
            <FormControl
              autoFocus
              type="email"
              value={this.state.emailLogin}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="passwordLogin" bsSize="large" className="text-left">
            <ControlLabel className="form-label">Password</ControlLabel>
            <FormControl
              value={this.state.passwordLogin}
              onChange={this.handleChange}
              type="password"
            />
          </FormGroup>
          <LoaderButton
            block
            bsSize="large"
            disabled={!this.validateForm()}
            type="submit"
            isLoading={this.state.isLoading}
            text="Login"
            className="form-submit"
            loadingText="Logging in…"
          />
        </form>
      </div>
    );
  }
}
