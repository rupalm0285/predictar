import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import { Nav, NavItem, Navbar, Image } from "react-bootstrap";
import Routes from "./Routes";
import { authUser, signOutUser } from "./libs/awsLib";
import RouteNavItem from "./components/RouteNavItem";
import LogoImage from "./image/predictkar-logo.jpeg";
import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isAuthenticated: false,
      isAuthenticating: true
    };
  }

  async componentDidMount() {
    try {
      if (await authUser()) {
        this.userHasAuthenticated(true);
      }
    }
    catch(e) {
      alert(e);
    }

    this.setState({ isAuthenticating: false });
  }

  userHasAuthenticated = authenticated => {
    this.setState({ isAuthenticated: authenticated });
  }

  handleLogout = event => {
    signOutUser();

    this.userHasAuthenticated(false);

    this.props.history.push("/");
  }

  render() {
    const childProps = {
      isAuthenticated: this.state.isAuthenticated,
      userHasAuthenticated: this.userHasAuthenticated
    };

    return (
      !this.state.isAuthenticating &&
      <div className="App container-fluid">
        <Navbar fixedTop fluid collapseOnSelect>
          <Navbar.Header>
            <Navbar.Brand>
              <Link to="/" className="logo"><Image responsive src="https://s3.ap-south-1.amazonaws.com/predictkar-uploads/predictkar-logo.png" /></Link>
            </Navbar.Brand>
            <Navbar.Toggle />        
          </Navbar.Header>
          <Navbar.Collapse>
            <Nav pullRight>
              {this.state.isAuthenticated
                ? [ 
                    <RouteNavItem key={1} href="/faq" className="primary-link-new">
                      FAQ
                    </RouteNavItem>,
                    <NavItem key={2} onClick={this.handleLogout} className="primary-link-new">Logout</NavItem>
                  ]
                : [
                    <RouteNavItem key={1} href="/faq" className="primary-link-new">
                      FAQ
                    </RouteNavItem>,
                    <RouteNavItem key={2} href="/contact-us" className="primary-link-new">
                      Contact Us
                    </RouteNavItem>
                  ]}
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Routes childProps={childProps} />
      </div>
    );
  }
}

export default withRouter(App);
